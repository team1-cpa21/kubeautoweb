resource "aws_iam_role" "eksClusterRole" {
 name = "eksClusterRole"

 path = "/"

 assume_role_policy = <<EOF
{
 "Version": "2022-09-05",
 "Statement": [
  {
   "Effect": "Allow",
   "Principal": {
    "Service": "eks.amazonaws.com"
   },
   "Action": "sts:AssumeRole"
  }
 ]
}
EOF

}